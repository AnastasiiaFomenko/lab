const express = require('express');
const app = express();
const path = require('path');
const fs = require('fs').promises;
const morgan = require('morgan');

app.use(morgan('tiny'));
app.use(express.json());

app.use((res, req, next) => {
    (async () => {
        try {
            await fs.mkdir(path.join(__dirname, 'files'), (err) => {
                if (err) {
                    throw err;
                }
            })

      } catch (err) {}
    })()
next()
})

app.get('/api/files/:filename', (req, res) => {
    (async () => {
        let filename = await req.params.filename;
        let content;
        try {
            let { birthtime } = await fs.stat(path.resolve(`./files/${filename}`));
            content = await fs.readFile(path.resolve(`./files/${filename}`), 'utf8');
            res.status(200).json({
                "message": "Success",
                "filename": filename,
                "content": content,
                "extension": path.extname(path.resolve(`./files/${filename}`)).substr(1),
                "uploadedDate": birthtime
            })
        } catch (err) {
            if (!content) {
                res.status(400).json({
                    "message": `No file with '${filename}' filename found`
                })
            }
        }
    })()

});

app.get('/api/files', (req, res) => {
    (async () => {
        let dirdata = await fs.readdir(path.resolve('./files'));
        res.status(200).json({
            "message": "Success",
            "files": dirdata
        })
    })()

});


app.post('/api/files', (req, res) => {
    (async () => {
        let filename = await req.body.filename;
        if (!filename || !filename.match(/[^:\/\\ \|\*\?><\"]*[\.](log|txt|json|yaml|xml|js|)$/)) {
            res.status(400).json({ "message": "Please specify 'filename' parameter" });
        } else if (!req.body.content) {
            res.status(400).json({ "message": "Please specify 'content' parameter" });
        } else {
            await fs.writeFile(path.join(__dirname, 'files', filename), req.body.content, err => {
                if (err) {
                    throw err
                }
            })
            res.status(200).json({ "message": "File created successfully" });
        }
    })()
})

app.use((err, req, res, next) => {
    res.status(500).send({ message: 'Server error' });
});

app.listen(8080);